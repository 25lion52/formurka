﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour 
{
    public State CorrectState;
    public int CorrectGesture;
    public LineRenderer lineRenderer;

    [Header("Screens")]
    public GameObject MainMenu;
    public GameObject GameMenu;
    public GameObject FailMenu;
    public GameObject RecordMenu;

    [Header("MainScreen")]
    public GameObject StartButton;
    public GameObject RecordGestureButton;

    [Header("GameScreen")]
    public GameObject ScoreLabel;
    public GameObject TimeLabel;

    [Header("LoseScreen")]
    public GameObject ScoreLabelLose;

    private float timer;
    private int score;
    private float SCALE_TIME = 0.75f;
    private float START_TIME = 10f;
    private float bonusTime = 3f;

    public enum State
    {
        Menu,
        Record,
        Play,
        Fail
    }

    private static UIManager _instance;
    public static UIManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UIManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        if (CorrectState == State.Play)
        {
            if (timer >= 0)
            {
                timer -= Time.deltaTime;
                TimeLabel.GetComponent<Text>().text = Mathf.Round(timer).ToString();
            }
            else
            {
                Lose();
            }
        }
    }

    public void PlayStart()
    {
        MainMenu.SetActive(false);
        FailMenu.SetActive(false);
        GameMenu.SetActive(true);
        CorrectState = State.Play;
        PassGesture();
        timer = START_TIME;
        score = 0;
        ScoreLabel.GetComponent<Text>().text = score.ToString();
    }

    public void PassGesture()
    {
        if (CorrectState == State.Play)
        {
            bonusTime *= SCALE_TIME;
            timer += bonusTime;
            score++;
            ScoreLabel.GetComponent<Text>().text = score.ToString();
            NewGesture();
        }
    }

    public void MenuScreen()
    {
        MainMenu.SetActive(true);
        FailMenu.SetActive(false);
        GameMenu.SetActive(false);
        RecordMenu.SetActive(false);
        lineRenderer.enabled = false;
        GestureManager.instance.ResetTempTemlate();
        CorrectState = State.Menu;
    }

    public void DrawGesture(ArrayList points)
    {
        lineRenderer.enabled = true;
        for (int i = 0; i < points.Count; i++)
        {
            Vector2 vv = (Vector2)points[i];
            lineRenderer.SetPosition(i, new Vector3(vv.x / 10, vv.y / 10, 0));
        }
    }

    public void RecordScreen()
    {
        RecordMenu.SetActive(true);
        MainMenu.SetActive(false);
        CorrectState = State.Record;
        lineRenderer.enabled = false;
        for (int i = 0; i < 64; i++)
            lineRenderer.SetPosition(i, Vector3.zero);
    }

    public void Exit()
    {
        Application.Quit();
    }

    private void Lose()
    {
        CorrectState = State.Fail;
        MainMenu.SetActive(false);
        FailMenu.SetActive(true);
        GameMenu.SetActive(false);
        lineRenderer.enabled = false;
        ScoreLabelLose.GetComponent<Text>().text = score.ToString();
    }

    private void NewGesture()
    {
        CorrectGesture = Random.Range(0, GestureTemplates.Templates.Count);
        DrawGesture((ArrayList)GestureTemplates.Templates[CorrectGesture]);
        
    }
}
