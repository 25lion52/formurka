using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Gesture : MonoBehaviour
{
    static GameObject gestureDrawing;

    ArrayList pointArr;
    static bool mouseDown;

    void Start ()
    {
	    pointArr = new ArrayList();
	    gestureDrawing = gameObject;
    }

    void test()
    {
       /* BinaryFormatter formatter = new BinaryFormatter();
        using(var fStream = new FileStream(Application.dataPath + "/SuperHumanInfo.dat", FileMode.Create, FileAccess.Write, FileShare.None))
        {
            formatter.Serialize(fStream, GestureTemplates.TemplateNames);
        }

        using(var fStream = File.OpenRead(Application.dataPath + "/SuperHumanInfo.dat"))
        {
            GestureTemplates.TemplateNames = (ArrayList)formatter.Deserialize(fStream);
        }
        
        XmlSerializer xml = new XmlSerializer(typeof(ArrayList));
        using (var fStream = new FileStream(Application.dataPath + "/SuperHumanInfo.dat", FileMode.Create, FileAccess.Write, FileShare.None))
        {
            xml.Serialize(fStream, GestureTemplates.TemplateNames);
        }*/
    }


    IEnumerator worldToScreenCoordinates ()
    {
	    Vector3 screenSpace = Camera.main.WorldToScreenPoint(gestureDrawing.transform.position);
    	
	    while (Input.GetMouseButton(0))
	    {
		    Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
		    Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace);
            curPosition.z = 0;
		    gestureDrawing.transform.position = curPosition;
		    yield return 0;
	    }
    }

    void Update()
    {
	    if (Input.GetMouseButtonDown(0))
        {
            UIManager.instance.lineRenderer.enabled = false;
		    mouseDown = true;
	    }
    	
	    if ((mouseDown) && 
            ((UIManager.instance.CorrectState == UIManager.State.Record) ||
            (UIManager.instance.CorrectState == UIManager.State.Play)))
        {
		    Vector2 p = new Vector2(Input.mousePosition.x , Input.mousePosition.y);
		    pointArr.Add(p);
		    StartCoroutine(worldToScreenCoordinates());
	    }


	    if (Input.GetMouseButtonUp(0))
        {
		    if (UIManager.instance.CorrectState == UIManager.State.Record)
            {
			    mouseDown = false;
			    GestureManager.instance.recordTemplate(pointArr);    		
		    }
            else if (UIManager.instance.CorrectState == UIManager.State.Play)
            {
			    mouseDown = false;
                GestureManager.instance.startRecognizer(pointArr);    		
		    }


            pointArr.Clear();
	    }
    } 
}
