﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GestureManager : MonoBehaviour 
{
    private int maxPoints = 64;
    private int sizeOfScaleRect = 500;
    private ArrayList newTemplateArr;

    private static GestureManager _instance;
    public static GestureManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GestureManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    public void startRecognizer(ArrayList pointArray)
    {
        pointArray = optimizeGesture(pointArray, maxPoints);
        pointArray = ScaleGesture(pointArray, sizeOfScaleRect);
        pointArray = TranslateGestureToOrigin(pointArray);
        gestureMatch(pointArray);
    }

    public void recordTemplate(ArrayList pointArray)
    {
        pointArray = optimizeGesture(pointArray, maxPoints);
        pointArray = ScaleGesture(pointArray, sizeOfScaleRect);
        pointArray = TranslateGestureToOrigin(pointArray);

        newTemplateArr = new ArrayList();
        newTemplateArr = pointArray;

        UIManager.instance.DrawGesture(newTemplateArr);

    }

    public void saveNewTemplate()
    {
        if (newTemplateArr != null)
        {
            GestureTemplates.Templates.Add(newTemplateArr);
            GestureTemplates.Save();
        }
    }

    public void ResetTempTemlate()
    {
        newTemplateArr = null;
    }

    private ArrayList optimizeGesture(ArrayList pointArray, int maxPoints)
    {
        float interval = calcTotalGestureLength(pointArray) / (maxPoints - 1);

        ArrayList optimizedPoints = new ArrayList();
        optimizedPoints.Add(pointArray[0]);

        float tempDistance = 0.0f;

        for (int i = 1; i < pointArray.Count; ++i)
        {
            float currentDistanceBetween2Points = calcDistance((Vector2)pointArray[i - 1], (Vector2)pointArray[i]);

            if ((tempDistance + currentDistanceBetween2Points) >= interval)
            {
                Vector2 v1 = (Vector2)pointArray[i - 1];
                Vector2 v = (Vector2)pointArray[i];

                float newX = v1.x + ((interval - tempDistance) / currentDistanceBetween2Points) * (v.x - v1.x);
                float newY = v1.y + ((interval - tempDistance) / currentDistanceBetween2Points) * (v.y - v1.y);

                Vector2 newPoint = new Vector2(newX, newY);

                optimizedPoints.Add(newPoint);

                ArrayList temp = pointArray.GetRange(i, pointArray.Count - i - 1);
                Vector2 last = (Vector2)pointArray[pointArray.Count - 1];
                pointArray.SetRange(i + 1, temp);
                pointArray.Add(last);
                pointArray.Insert(i, newPoint);

                tempDistance = 0.0f;
            }
            else
            {
                tempDistance += currentDistanceBetween2Points;
            }
        }

        if (optimizedPoints.Count == maxPoints - 1)
        {
            Vector2 v = (Vector2)pointArray[pointArray.Count - 1];
            optimizedPoints.Add(new Vector2(v.x, v.y));
        }

        return optimizedPoints;
    }

    private float calcTotalGestureLength(ArrayList pointArray)
    {
        float length = 0.0f;
        for (int i = 1; i < pointArray.Count; ++i)
        {
            length += calcDistance((Vector2)pointArray[i - 1], (Vector2)pointArray[i]);
        }

        return length;
    }

    private float calcDistance(Vector2 point1, Vector2 point2)
    {
        float dx = point2.x - point1.x;
        float dy = point2.y - point1.y;

        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    private Vector2 calcCenterOfGesture(ArrayList pointArray)
    {
        float averageX = 0.0f;
        float averageY = 0.0f;

        foreach (Vector2 v in pointArray)
        {
            averageX += v.x;
            averageY += v.y;
        }

        averageX = averageX / pointArray.Count;
        averageY = averageY / pointArray.Count;

        return new Vector2(averageX, averageY);
    }

    private ArrayList ScaleGesture(ArrayList pointArray, int size)
    {
        float minX = Mathf.Infinity;
        float maxX = Mathf.NegativeInfinity;
        float minY = Mathf.Infinity;
        float maxY = Mathf.NegativeInfinity;

        foreach (Vector2 v in pointArray)
        {
            if (v.x < minX) minX = v.x;
            if (v.x > maxX) maxX = v.x;
            if (v.y < minY) minY = v.y;
            if (v.y > maxY) maxY = v.y;
        }

        Rect BoundingBox = new Rect(minX, minY, maxX - minX, maxY - minY);
        ArrayList newArray = new ArrayList();

        foreach (Vector2 v in pointArray)
        {
            float newX = v.x * (size / Mathf.Max(BoundingBox.height, BoundingBox.width));
            float newY = v.y * (size / Mathf.Max(BoundingBox.height, BoundingBox.width));
            newArray.Add(new Vector2(newX, newY));
        }

        return newArray;
    }

    private ArrayList TranslateGestureToOrigin(ArrayList pointArray)
    {
        Vector3 center = calcCenterOfGesture(pointArray);
        ArrayList newArray = new ArrayList();

        foreach (Vector2 v in pointArray)
        {
            float newX = v.x - center.x;
            float newY = v.y - center.y;
            newArray.Add(new Vector2(newX, newY));
        }

        return newArray;
    }

    private int getLeftPoint(ArrayList pointArray)
    {
        float minX = float.MaxValue;
        int result = 0;

        for (int i = 0; i < pointArray.Count; i++)
        {
            Vector2 newPoint = (Vector2)pointArray[i];
            if (newPoint.x < minX)
            {
                result = i;
                minX = newPoint.x;
            }
        }

        return result;
    }

    private void gestureMatch(ArrayList pointArray)
    {
        float tempDistance = Mathf.Infinity;
        int count = 0;

        for (int i = 0; i < GestureTemplates.Templates.Count; ++i)
        {
            float distance = oneRound(pointArray,
                (ArrayList)GestureTemplates.Templates[i],
                getLeftPoint(pointArray),
                getLeftPoint((ArrayList)GestureTemplates.Templates[i]));

            if (distance < tempDistance)
            {
                tempDistance = distance;
                count = i;
            }
        }

        float HalfDiagonal = 0.5f * Mathf.Sqrt(Mathf.Pow(sizeOfScaleRect, 2) + Mathf.Pow(sizeOfScaleRect, 2));
        float score = 1.0f - (tempDistance / HalfDiagonal);

        if (score < 0.8f)
        {
            Debug.Log("NO MATCH " + score);
        }
        else
        {
            Debug.Log("RESULT: " + count + " SCORE: " + score);
            if(count == UIManager.instance.CorrectGesture)
                UIManager.instance.PassGesture();
        }

    }

    private float oneRound(ArrayList firstArr, ArrayList secondArr, int firstStart, int secondStart)
    {
        int counter1 = firstStart;
        int counter2 = secondStart;
        float sum = 0;
        bool wrongVector = false;

        for (int i = 0; i < firstArr.Count; i++)
        {
            float dist = calcDistance((Vector2)firstArr[counter1], (Vector2)secondArr[counter2]);
            if (dist > 200)
            {
                wrongVector = true;
                break;
            }
            sum += dist;
            counter1++;
            counter2++;

            if (counter1 >= firstArr.Count)
                counter1 = 0;
            if (counter2 >= secondArr.Count)
                counter2 = 0;
        }

        if (wrongVector)
        {
            counter1 = firstStart;
            counter2 = secondStart;
            sum = 0;
            for (int i = 0; i < firstArr.Count; i++)
            {
                float dist = calcDistance((Vector2)firstArr[counter1], (Vector2)secondArr[counter2]);
                sum += dist;
                counter1--;
                counter2++;

                if (counter1 < 0)
                    counter1 = firstArr.Count - 1;
                if (counter2 >= secondArr.Count)
                    counter2 = 0;
            }
        }

        sum /= firstArr.Count;
        return sum;
    }
}
