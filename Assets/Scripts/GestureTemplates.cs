using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

// all known gesture templates
[Serializable]
public static class GestureTemplates
{
    [XmlArray]
    public static ArrayList Templates;

    static GestureTemplates ()
    {
        Templates = new ArrayList();
        try
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (var fStream = File.OpenRead(Application.dataPath + "/Templates.dat"))
            {
                var templ = (ArrayList)formatter.Deserialize(fStream);

                foreach (ForSerializeObj obj in templ)
                {
                    ArrayList list = new ArrayList();
                    foreach (ForSerializePoint p in obj.points)
                    {
                        list.Add(new Vector2(p.x, p.y));
                    }

                    Templates.Add(list);
                }
            }
        }
        catch (Exception)
        {
            Debug.Log("File not found");
        }
    }

    public static void Save()
    {
        ArrayList newlist = new ArrayList();
        foreach(ArrayList arr in Templates)
        {
            ArrayList newPoints = new ArrayList();
            foreach (Vector2 vv in arr)
            {
                newPoints.Add(new ForSerializePoint() { x = vv.x, y = vv.y });
            }
            newlist.Add(new ForSerializeObj() { points = newPoints });
        }

        BinaryFormatter formatter = new BinaryFormatter();
        using (var fStream = new FileStream(Application.dataPath + "/Templates.dat", FileMode.Create, FileAccess.Write, FileShare.None))
        {
            formatter.Serialize(fStream, newlist);
        }
    }

    [Serializable]
    class ForSerializeObj
    {
        public ArrayList points;
    }

    [Serializable]
    class ForSerializePoint
    {
        [SerializeField]
        public float x;
        [SerializeField]
        public float y;
    }
}